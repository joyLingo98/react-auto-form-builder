import React from 'react';

const Footer = ({columns}) => {
    const tbTitles = Object.keys(columns).map(key=><th scope="col">{columns[key]}</th>);
    return(
        <tfoot className="thead-dark">
        <tr>
            {tbTitles}
        </tr>
        </tfoot>
    )
};
export default Footer;