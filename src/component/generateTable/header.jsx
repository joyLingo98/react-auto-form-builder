import React from 'react';

const Header = ({columns}) => {
const thTitles = Object.keys(columns).map(key=><th scope="col">{columns[key]}</th>);
    return(
        <thead className="thead-dark">
        <tr>
            {thTitles}
        </tr>
        </thead>
    )
};
export default Header;