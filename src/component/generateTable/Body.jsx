import React from 'react';

const Body = ({data}) => {
    const rows = data.map(item => {
        const row = Object.keys(item).map(key => {
            return <td>{item[key]}</td>
        });
        return <tr>{row}</tr>
    });
    return (
        <tbody>{rows}</tbody>
    )
};
export default Body;